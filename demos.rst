.. _demos_ipynbs:

====================
Demonstration ipynbs
====================

There are jupyter notebook demonstrations of some key functionalities of hat EMS:

Transmission system model development, power flow, state estimation from early stage:
    - `basic test case <https://gitlab.com/jkrstulo/hat_ems/blob/master/demo_ipynbs/NDC_pnet_v3_test.ipynb>`_