.. _bus:

===========
Bus segment
===========

The bus segment is a one-terminal element used to represent the physical busbar section. After topology processing of
all the breaker statuses, bus segments are eventually merged to represent electrical nodes, i.e. buses of a bus/branch
power system model.