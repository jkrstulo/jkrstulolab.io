
=======================
Transformer (2-winding)
=======================

Model of any 2-winding transformer can be represented with the general branch model, with the following specific properties:

* charging conductance :math:`g_c` is half of the total transformer magnetizing conductance :math:`g_c=g_m/2`
* charging susceptance :math:`b_c` is half of the total line transformer magnetizing susceptance :math:`b_c=b_m/2`

Parameters of a 2-winding transformer are calculated as follows (in p.u.):



**Series impedance**

.. math::
   :nowrap:

   \begin{align*}

   z=& \frac{u_{k\%}}{100} \cdot \frac{U_n^2}{S_n} \cdot \frac{1}{Z_{base}} \\

   r=& P_k \cdot \frac{U_n^2}{S_n} \cdot \frac{1}{Z_{base}} \\

   x=& \sqrt{z^2 - r^2}

   \end{align*}


**Series impedance**

.. math::
   :nowrap:

   \begin{align*}

   y_m=& \frac{i_0}{100} \cdot \frac{S_n}{U_n^2} \cdot \frac{1}{Z_{base}} \\

   g_m=& \frac{P_0}{100} \cdot \frac{1}{U_n^2} \cdot \frac{1}{Z_{base}} \\

   b_m=& - \sqrt{y_m^2 - g_m^2}

   \end{align*}


where base impedance is :math:`Z_{base} = \frac{U_{base}^2}{S_{base}}`