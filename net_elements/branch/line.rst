.. _line:


====
Line
====


Model of any line can be represented with the general branch model from Fig. 2 with the following specific properties:

* tap ratio :math:`a=1`
* charging conductance :math:`g_c` is half of the total line charging conductance
* charging susceptance :math:`b_c` is half of the total line charging susceptance

Branch model parameters are calculated from line impedance, charging conductance and line length
(and transformed into per unit system) as the following:

.. math::
   :nowrap:

   \begin{align*}
    z_s &= (\left( r\_ohm\_per\_km  + j \cdot x\_ohm\_per\_km \right) \cdot length\_km ) / Z_{base} \\

    y_c &= \left( g\_siemens\_per\_km + j \cdot b\_siemens\_per\_km \right) \cdot (length\_km)/2 \cdot Z_{base}

    \end{align*}

where base impedance is calculated from the base voltage :math:`U_{base}` at the to bus (bus2id) and the system-wide defined
rated apparent power :math:`S_{base}`:

.. math::

   Z_{base}=\frac{U_{base}^2}{S_{base}}




Input Parameters
================

.. tabularcolumns:: |p{0.15\linewidth}|p{0.10\linewidth}|p{0.15\linewidth}|p{0.20\linewidth}|
.. csv-table::
   :file: line_par.csv
   :delim: ,
   :widths: 15, 10, 12, 20



Create Function
===============






