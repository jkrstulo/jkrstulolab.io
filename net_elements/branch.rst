.. _branch:

========
Branches
========


All the two-terminal elements: transmission lines, transformers, phase shifters and series compensators are modeled with
a common :math:`\pi` branch model, consisting of a standard  equivalent model, with series admitance
:math:`y_s=1/(r_s+jx_s)` and total charging admitance :math:`y_c=g_c+jb_c`, in series with an ideal phase shifting
transformer located at the from end of the branch (see the following figure).
Ideal phase shifting transformer has tap ratio :math:`a` with magnitude :math:`t` and phase shift angle
:math:`\theta_{shift}` (:math:`a=te^{j\theta_{shift}}`).



.. figure:: branch.png
    :width: 25em
    :alt: alternate Text
    :figclass: align-center

    General branch model



Hereafter follows the documentation for each element type that is being represented as a branch in the bus/branch
network model:


.. toctree::
   :maxdepth: 1

   branch/line
   branch/transformer
   branch/transformer3w
   branch/series_compensator
