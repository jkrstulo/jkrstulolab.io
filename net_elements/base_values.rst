
===========================
Base Values and conventions
===========================


Base Voltage
============

:math:`U_{base}` - defined for all elements running at the same nominal voltage

Base Power
==========

:math:`S_{base}` System-wide defined power. It is defined in `pnet.sn_kva` with default values :math:`1000 kVA`


Base impedance
==============

.. math::

    Z_{base}=\frac{U_{base}^2}{S_{base}}