.. _data_io:

==================
Load and Save Data
==================

The following functions are related to reading and writing input or output data to different formats.


Load Data from postgres db
--------------------------

.. autofunction:: hatems.from_db.postgres_to_files.postgres_to_pandas_to_csv