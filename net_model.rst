.. _net_model:

##################
Network model
##################

This section provides details about general network modeling, with references to specific issues of each
power system analysis.
The power network object is structured with a table for each relevant element type, where each table contains
corresponding element parameters.

For each each element type, detailed information is given about parameters, modelling and their interpretation in
respect to specific analysis:

.. note::

    All the power system analyses in this document assume system operation in the steady state under balanced conditions.
    This implies that all bus loads and branch power flows will be three phase and balanced, all transmission
    lines are fully transposed and all other series or shunt devices are symmetrical in the three phases.
    These assumptions allow the use of the single phase positive sequence equivalent circuit for modeling
    the entire power system.


.. toctree::
   :maxdepth: 2

   net_elements/base_values
   net_elements/branch
   net_elements/bus_segment
   net_elements/generator
   net_elements/load
   net_elements/shunt_comp
   net_elements/net_equations




