
==================
Topology Processor
==================


The topology processor (TP) engine has a general objective to generate network graph from considering relevant network
information. More specifically, TP determines the bus/branch model of a network from the bus section/circuit breaker model
(a model considering all circuit breakers and their statuses). In simple words it provides the network graph with
the lowest possible number of buses, after eliminating all branches corresponding to closed circuit breakers
(buses at the ends of the closed circuit breaker are merged to a single bus).


Specific tasks of the topology processor are the following:

* Represent the network with minimum number of identified buses.
* Identification of all the network elements connected to the identified buses
* Identification of network connectivity: eventually multiple islands.
* Identification of the bus types: PQ, PV and slack bus(es) (optional)

Generally, the NTP identifies all the network elements as the following:

* **branch** – edge of a graph which contains a list of corresponding parallel branches (it can be any 2-terminal element: line, transformer, circuit breaker, series compensator, etc.)
* **node** – vertex of a graph which contains a list of all connected shunt elements


.. _run_tp:

.. autofunction:: hatems.topology_processor.run_tp


